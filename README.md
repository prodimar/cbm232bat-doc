# CBM232BAT: instrucciones para la actualización de firmware

## Consulta de la versión de firmware

El software de terminal de libre distribución **Hercules Setup** utilizado en estas capturas de pantalla puede descargarse [aquí](https://www.hw-group.com/software/hercules-setup-utility).

1. Conectar el dispositivo a un PC, vía USB, determinar en el Administrador de Dispositivos el puerto asignado y abrir dicho puerto con el terminal Hercules (la velocidad y demás parámetros del puerto no son significativos).

2. En el terminal Hercules (habilitar vista "_HEX Enable_") utilizando el botón derecho en pantalla principal.

3. Enviar comando [en **HEX**]:

    `1B 02 0F 00`

4.  El comando devuelve, en secuencia: 
    * Versión de hardware.
    * Versión de firmware.
    * Numero de serie del equipo.

En la captura de pantalla inferior:

    {05}{19}{31}{37}{32}{32}{30}{30}{35}{32}
     |   |  --------------------------------
     |   |   Número de serie (ASCII): 17220052
     |   |
     |   |__ Versión de firmware: 19
     |______ Versión de hardware: 05

![alt](https://bitbucket.org/prodimar/cbm232bat-doc/raw/master/media/screenshot.png "Determinación de la versión de firmware")

## Actualización de firmware

1. Desde el Hercules enviar comando [en HEX]

    `1B 02 0C`

2. Cerrar puerto COM
   (**en versiones modernas de Windows es necesario también desconectar rápidamente el cable USB, esperar 10 segundos y volverlo a conectar**)

3. El equipo se reenumerará como USB HID

4. Desde una línea de comandos **cmd** ejecutar el comando (ejecutable descargable de [aquí](https://bitbucket.org/prodimar/cbm232bat-doc/downloads/msp430-bsl5-flashfile.exe)):

        msp430-bsl5-flashfile.exe fichero_de_firmware
        Ejemplo: msp430-bsl5-flashfile.exe cbm232bat_hw05_fw22_20180508.txt

5. **IMPORTANTE**: Realizar el procedimiento de _Restauración de valores por defecto_ descrito en la sección siguiente.

![alt](https://bitbucket.org/prodimar/cbm232bat-doc/raw/master/media/screenshot_update_fw.png "Actualización de firmware")

## Restauración de valores por defecto

El procedimiento para restaurar el equipo a los valores de configuración por defecto consiste en encender el equipo con el botón de opciones (engranaje) pulsado. Por tanto:

1. (con el equipo apagado) Pulsar y mantener pulsado el botón de opciones.

2. Encender el equipo con el botón de on/off.

3. El equipo encenderá momentáneamente los 2 leds y se reiniciará restaurando sus configuración por defecto.

## Lista de firmwares disponibles

* [versión 22](https://bitbucket.org/prodimar/cbm232bat-doc/downloads/cbm232bat_hw05_fw22_20180508.txt)
